package ictgradschool.web.lab14.ex1;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Exercise01_Prompt {
    public static void prompt() {

        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties())) {
            System.out.println("Connection successful");
            try(PreparedStatement stat = conn.prepareStatement(
                    "SELECT body FROM simpledao_articles WHERE title LIKE ?;"
            )){
                String input= "";
                while(input.equals("")){
                    System.out.print("Please enter a title or quit to stop:");
                    input = Keyboard.readInput().toLowerCase();
                }
                stat.setString(1,input+"%");
                try(ResultSet results = stat.executeQuery()){
                    while (results.next()){
                        String text = results.getString(1);
                        System.out.println("Here is the text: " + text);
                        System.out.println();
                    }
                    if (!results.next()){
                        if (!input.equals("quit")){
                            prompt();
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        prompt();
    }
}
