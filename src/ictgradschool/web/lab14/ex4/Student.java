package ictgradschool.web.lab14.ex4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Student extends university{


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getMentor() {
        return mentor;
    }

    public void setMentor(int mentor) {
        this.mentor = mentor;
    }

    private int id;
    private String fname;
    private String lname;
    private String country;
    private int mentor;

    @Override
    public String getbasicinfo() {
        return ("Student Id: " + id + ", Name: "+ fname+ " "+lname + ", nationality: " + country + ", mentor number: "+mentor);
    }

    @Override
    public String abstractinfor() {
        return ("Student Id: " + id + ", Name: "+ fname+ " "+lname );
    }

}
