package ictgradschool.web.lab14.ex4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Course extends university {


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCoord_no() {
        return coord_no;
    }

    public void setCoord_no(int coord_no) {
        this.coord_no = coord_no;
    }

    public int getRep_id() {
        return rep_id;
    }

    public void setRep_id(int rep_id) {
        this.rep_id = rep_id;
    }


    public int getStudentid() {
        return Studentid;
    }

    public void setStudentid(int studentid) {
        Studentid = studentid;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    private String number;
    private String department;
    private String description;
    private int coord_no;
    private int rep_id;
    private int Studentid;
    private String semester;
    private String mark;


    @Override
    public String getbasicinfo() {
        String basicinfo = ("Course_number: " + number + ", Department: " + department + ", description: " + description + ", coord_no: " + coord_no + ", rep_id: " + rep_id + ", Student_id: " + Studentid + ", Semester_Attended: " + semester + ", marks: " + mark);
        String info = basicinfo.replaceAll(", \\w++: (null|0)", " ");
        return info;
    }

    @Override
    public String abstractinfor() {
        String basicinfo = ("Course_number: " + number + ", Department: " + department);
        String info = basicinfo.replaceAll(", \\w++: (null|0)", " ");
        return info;
    }

}
