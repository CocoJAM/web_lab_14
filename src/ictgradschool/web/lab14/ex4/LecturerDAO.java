package ictgradschool.web.lab14.ex4;

import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex3.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 11/05/2017.
 */
public class LecturerDAO {
    List<university> listOfuni = new ArrayList<>();
    public List<university> getListOfuni1() {
        try(PreparedStatement stat = conn.prepareStatement(
                "SELECT * FROM unidb_lecturers"
        )){
            try (ResultSet results = stat.executeQuery()) {
                while (results.next()) {
                    Lecturer a = new Lecturer();
                    a.setStaff_no(results.getInt(1));
                    a.setFname(results.getString(2));
                    a.setLname(results.getString(3));
                    a.setOffice(results.getString(4));
                    listOfuni.add(a);
                }

            }

        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return listOfuni;
    }

    public List<university> getListOfuni2() {
        List<university> listOfuni = new ArrayList<>();
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_teach;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
                        Lecturer a = new Lecturer();
                        a.setStaff_no(results.getInt(3));
                        a.setDept(results.getString(1));
                        a.setNumber(results.getString(2));
                        listOfuni.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }


    private Connection conn;

    public LecturerDAO() {

        try  {Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties());
            this.conn = conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
