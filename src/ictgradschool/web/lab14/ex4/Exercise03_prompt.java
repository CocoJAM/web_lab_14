package ictgradschool.web.lab14.ex4;

import ictgradschool.Keyboard;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ljam763 on 4/05/2017.
 */
public class Exercise03_prompt {
    private static String input;
    private Connection conn;

    public static String getContext() {
        return context;
    }

    private static String context;

    public static String context(int inputnumber){
        context = "";
        switch (inputnumber) {
            case 1:
                context = "unidb_courses";
                return context;
            case 2:
                context = "unidb_attend";
                return context;
            case 3:
                context = "unidb_lecturers";
                return context;
            case 4:
                context = "unidb_teach";
                return context;
            case 5:
                context = "unidb_students";
                return context;
            case 6:
                context = null;
                return context;
        }
        return context;
    }


    public static void startingPage(){
        System.out.println("Connection successful");
        System.out.println("Welcome to the University database!!!");
        System.out.println("1. Information about offered courses.");
        System.out.println("2. Information about the courses were attended by students.");
        System.out.println("3. Information about our high achieving lecturers.");
        System.out.println("4. Information about lecturers taught courses");
        System.out.println("5. Information about our attending student");
        System.out.println("6. For Exiting");
    }


    public static List<university> prompt() {
        List<university> listOfuni = new ArrayList<>();
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        startingPage();
        String input = Keyboard.readInput();
        if (input.isEmpty()){
            System.out.println("Empty");
            return null;
        }
        String type_of_database =  context( Integer.parseInt(input));

        CourseDAO courseDAO = new CourseDAO();
        LecturerDAO lecturerDAO = new LecturerDAO();
        StudentDAO studentDAO = new StudentDAO();


        switch (Integer.parseInt(input)) {
            case 1:
                listOfuni = courseDAO.getListOfuni1();
                break;
            case 2:
                listOfuni = courseDAO.getListOfuni2();
                break;
            case 3:
                listOfuni = lecturerDAO.getListOfuni1();
                break;
            case 4:
                listOfuni = lecturerDAO.getListOfuni2();
                break;
            case 5:
                listOfuni = studentDAO.getListOfuni();
                break;
            case 6:
                context = null;
                listOfuni.removeAll(listOfuni);
                return null;
        }
        System.out.println("Welcome to "+ type_of_database);
        System.out.println(listOfuni.size());
        return listOfuni;
    }


    public static void main(String[] args) {
        List<university> listofthings = new ArrayList<>();
        while(listofthings.isEmpty()){
            listofthings = prompt();
            iteratetor list = new iteratetor(listofthings);
            list.selection();
            if ( getContext() == null){
                System.out.println("done");
                return;
            }
        }
    }
}
