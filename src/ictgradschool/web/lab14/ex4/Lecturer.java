package ictgradschool.web.lab14.ex4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Lecturer extends university{

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getStaff_no() {
        return staff_no;
    }

    public void setStaff_no(int staff_no) {
        this.staff_no = staff_no;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }


    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    private String fname;
    private String lname;
    private int staff_no;
    private String office;
    private String dept;
    private String number;

    @Override
    public String getbasicinfo() {
        String basicinfo = (", First_name: " + fname + ", Last_name: "+ lname + ", staff_no: "+ staff_no + ", office: " + office+", Course_Number: "+ number + ", Course_department: "+ dept);
        String info = basicinfo.replaceAll(" \\w++: (null|0)" , "").replaceAll(",{1,}", "");

        return info;
    }

    @Override
    public String abstractinfor() {
        String basicinfo = (", staff_no: "+ staff_no + ", office: " + office);
        String info = basicinfo.replaceAll(" \\w++: (null|0)" , "").replaceAll(",{1,}", "");
        return info;
    }

}
