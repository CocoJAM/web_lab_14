package ictgradschool.web.lab14.ex4;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex3.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 11/05/2017.
 */
public class CourseDAO {
    public CourseDAO() {
        try{Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties());
            this.conn = conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<university> getListOfuni1() {
        List<university> listOfuni = new ArrayList<>();
                try (PreparedStatement stat = conn.prepareStatement(
                        "SELECT * FROM unidb_courses;"
                )) {
                    try (ResultSet results = stat.executeQuery()) {
                        while (results.next()) {
                            Course a = new Course();
                            a.setNumber(results.getString(2));
                            a.setDepartment(results.getString(1));
                            a.setCoord_no(results.getInt(4));
                            a.setDescription(results.getString(3));
                            a.setRep_id(results.getInt(5));
                            listOfuni.add(a);
                        }
                    }
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
        return listOfuni;
    }

        public List<university> getListOfuni2() {
            List<university> listOfuni = new ArrayList<>();
                try {
                    try (PreparedStatement stat = conn.prepareStatement(
                            "SELECT * FROM unidb_attend;"
                    )) {
                        try (ResultSet results = stat.executeQuery()) {
                            while (results.next()) {
                                Course a = new Course();
                                a.setNumber(results.getString(3));
                                a.setSemester(results.getString(4));
                                a.setMark(results.getString(5));
                                a.setDepartment(results.getString(2));
                                a.setStudentid(results.getInt(1));
                                listOfuni.add(a);
                            }
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            return listOfuni;
        }

    private static Connection conn;

    private List<university> uni = new ArrayList<>();


}
