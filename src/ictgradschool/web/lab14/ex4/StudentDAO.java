package ictgradschool.web.lab14.ex4;

import ictgradschool.web.lab14.Config;

import ictgradschool.web.lab14.ex4.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 11/05/2017.
 */
public class StudentDAO {
    List<university> listOfuni = new ArrayList<>();
    public List<university> getListOfuni() {
        try (PreparedStatement stat = conn.prepareStatement(
                "SELECT * FROM unidb_students;"
        )) {
            try (ResultSet results = stat.executeQuery()) {
                while (results.next()) {
                    Student c = new Student();
                    c.setId(results.getInt(1));
                    c.setFname(results.getString(2));
                    c.setLname(results.getString(3));
                    c.setCountry(results.getString(4));
                    c.setMentor(results.getInt(5));
                    listOfuni.add(c);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }

    private Connection conn;

    public Connection getConn() {
        return conn;
    }


    public StudentDAO() {
        try  {Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties());
            System.out.println("Connection successful");
            this.conn = conn;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}

