package ictgradschool.web.lab14.ex2;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Exercise02_prompt {
    public static void prompt() {
        System.out.println("Please enter the title");
        String input = Keyboard.readInput();
        List<Articles> a = new ArticlesDAO(input).getGained();
        for (Articles articles : a) {
            System.out.println("Title: "+ articles.getTitle()+". Text Content: "+ articles.getText());
        }
    }

    public static void main(String[] args) {
        prompt();
    }
}
