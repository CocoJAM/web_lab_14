package ictgradschool.web.lab14.ex2;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class ArticlesDAO {
    public List<Articles> getGained() {
        return gained;
    }

    private List<Articles> gained = new ArrayList<>();
    public ArticlesDAO(String titles){
      try {
        Class.forName("com.mysql.jdbc.Driver");
    } catch (Exception e) {
        e.printStackTrace();
    }
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties())) {
            System.out.println("Connection successful");
            try(PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM simpledao_articles WHERE title LIKE ?;"
            )){
                stat.setString(1,titles+"%");
                try(ResultSet results = stat.executeQuery()){
                    while (results.next()){
                        Articles a = new Articles();
                        a.setArticleID(results.getInt(1));
                        a.setTitle(results.getString(2));
                        a.setText(results.getString(3));
                        gained.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
