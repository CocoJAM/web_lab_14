package ictgradschool.web.lab14.ex2;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Articles {
    private int ArticleID;
    private String Title;
    private String Text;

    public int getArticleID() {
        return ArticleID;
    }

    public void setArticleID(int articleID) {
        ArticleID = articleID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public Articles(int ID, String Title, String Text){
        this.ArticleID= ID;
        this.Title = Title;
        this.Text = Text;
    }
    public Articles(){}
}
