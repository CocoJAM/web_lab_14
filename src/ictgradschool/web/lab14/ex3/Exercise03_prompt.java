package ictgradschool.web.lab14.ex3;

import ictgradschool.Keyboard;
import ictgradschool.web.lab14.Config;
import ictgradschool.web.lab14.ex2.Articles;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ictgradschool.web.lab14.ex3.Course.choice1;
import static ictgradschool.web.lab14.ex3.Course.choice2;
import static ictgradschool.web.lab14.ex3.Lecturer.choice3;
import static ictgradschool.web.lab14.ex3.Lecturer.choice4;
import static ictgradschool.web.lab14.ex3.Student.choice5;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Exercise03_prompt {
    private static String input;
    private Connection conn;

    public static String getContext() {
        if (context!= null){
        return context;}
        else {
            return null;
        }
    }

    private static String context;

    public static String context(int inputnumber){
        context = "";
        switch (inputnumber) {
            case 1:
                context = "unidb_courses";
                return context;
            case 2:
                context = "unidb_attend";
                return context;
            case 3:
                context = "unidb_lecturers";
                return context;
            case 4:
                context = "unidb_teach";
                return context;
            case 5:
                context = "unidb_students";
                return context;
            case 6:
                context = "exit";
                return context;
        }
        return context;
    }


    public static void startingPage(){
        System.out.println("Connection successful");
        System.out.println("Welcome to the University database!!!");
        System.out.println("1. Information about offered courses.");
        System.out.println("2. Information about the courses were attended by students.");
        System.out.println("3. Information about our high achieving lecturers.");
        System.out.println("4. Information about lecturers taught courses");
        System.out.println("5. Information about our attending student");
        System.out.println("6. Exit");
    }


    public static List<university> prompt() {
        List<university> listOfuni = new ArrayList<>();
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/ljam763", Config.getProperties())) {
            startingPage();
            int input_number = 0;
            String context = "";
            while (input_number < 1 || input_number > 7) {
                System.out.println("Please select information from above: ");
                try{ input_number = Integer.parseInt(Keyboard.readInput());}
                catch (NumberFormatException e){
                    System.out.println("Please enter a number shown from above.");
                    input_number = Integer.parseInt(Keyboard.readInput());
                }
                context = context(input_number);
                System.out.println( context + " database");
            }
            if (!context.equals("exit")) {
                switch (input_number) {
                    case 1:
                        choice1(conn, listOfuni);
                        break;
                    case 2:
                        choice2(conn,listOfuni);
                        break;
                    case 3:
                        choice3(conn,listOfuni);
                        break;
                    case 4:
                        choice4(conn,listOfuni);
                        break;
                    case 5:
                        choice5(conn, listOfuni);
                        break;
                    case 6:
                        return null;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }


    public static void main(String[] args) {
        List<university> listofthings = new ArrayList<>();
        while(listofthings.isEmpty()){
            listofthings = prompt();
            iteratetor list = new iteratetor(listofthings);
            list.selection();
            if (getContext().equals("exit")){
                break;
            }
        }
    }
}
