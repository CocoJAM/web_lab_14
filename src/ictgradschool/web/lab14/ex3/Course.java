package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Course extends university {
    public Course(String number, String department, String description, int coord_no, int rep_id) {
        this.number = number;
        this.department = department;
        this.description = description;
        this.coord_no = coord_no;
        this.rep_id = rep_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCoord_no() {
        return coord_no;
    }

    public void setCoord_no(int coord_no) {
        this.coord_no = coord_no;
    }

    public int getRep_id() {
        return rep_id;
    }

    public void setRep_id(int rep_id) {
        this.rep_id = rep_id;
    }

    public Course(String number, String department, String description, int coord_no, int rep_id, int studentid, String semester, String mark) {
        this.number = number;
        this.department = department;
        this.description = description;
        this.coord_no = coord_no;
        this.rep_id = rep_id;
        Studentid = studentid;
        this.semester = semester;
        this.mark = mark;
    }

    public Course(String number, String department, int studentid, String semester, String mark) {
        this.number = number;
        this.department = department;
        Studentid = studentid;
        this.semester = semester;
        this.mark = mark;
    }

    public int getStudentid() {
        return Studentid;
    }

    public void setStudentid(int studentid) {
        Studentid = studentid;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    private String number;
    private String department;
    private String description;
    private int coord_no;
    private int rep_id;
    private int Studentid;
    private String semester;
    private String mark;


    @Override
    public String getbasicinfo() {
        String basicinfo = ("Course_number: " + number + ", Department: " + department + ", description: " + description + ", coord_no: " + coord_no + ", rep_id: " + rep_id + ", Student_id: " + Studentid + ", Semester_Attended: " + semester + ", marks: " + mark);
        String info = basicinfo.replaceAll(", \\w++: (null|0)", " ");
        return info;
    }

    @Override
    public String abstractinfor() {
        String basicinfo = ("Course_number: " + number + ", Department: " + department);
        String info = basicinfo.replaceAll(", \\w++: (null|0)", " ");
        return info;
    }

    public Course() {
    }

    public static List<university> choice1(Connection conn, List<university> listOfuni) {
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_courses;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
                        Course a = new Course();
                        a.setNumber(results.getString(2));
                        a.setDepartment(results.getString(1));
                        a.setCoord_no(results.getInt(4));
                        a.setDescription(results.getString(3));
                        a.setRep_id(results.getInt(5));
                        listOfuni.add(a);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }


    public static List<university> choice2(Connection conn, List<university> listOfuni) {
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_attend;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
//                        Course c = new Course(num, dept, id, sem, marks);
                        Course a = new Course();
                        a.setNumber(results.getString(3));
                        a.setSemester(results.getString(4));
                        a.setMark(results.getString(5));
                        a.setDepartment(results.getString(2));
                        a.setStudentid(results.getInt(1));
                        listOfuni.add(a);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }


    public static void main(String[] args) {
        Course a = new Course("87454", "comp", "asfsdfd", 12354, 87954);
        System.out.println(a.abstractinfor());

    }
}
