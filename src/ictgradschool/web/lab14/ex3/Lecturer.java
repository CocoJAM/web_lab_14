package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Lecturer extends university{
    public Lecturer(String fname, String lname, int staff_no, String office) {
        this.fname = fname;
        this.lname = lname;
        this.staff_no = staff_no;
        this.office = office;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getStaff_no() {
        return staff_no;
    }

    public void setStaff_no(int staff_no) {
        this.staff_no = staff_no;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }



    public Lecturer(int staff_no, String dept, String number) {
        this.staff_no = staff_no;
        this.dept = dept;
        this.number = number;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    private String fname;
    private String lname;
    private int staff_no;
    private String office;
    private String dept;
    private String number;

    @Override
    public String getbasicinfo() {
        String basicinfo = (", First_name: " + fname + ", Last_name: "+ lname + ", staff_no: "+ staff_no + ", office: " + office+", Course_Number: "+ number + ", Course_department: "+ dept);
        String info = basicinfo.replaceAll(" \\w++: (null|0)" , "").replaceAll(",{1,}", "");

        return info;
    }

    @Override
    public String abstractinfor() {
        String basicinfo = (", staff_no: "+ staff_no + ", office: " + office);
        String info = basicinfo.replaceAll(" \\w++: (null|0)" , "").replaceAll(",{1,}", "");
        return info;
    }
    public Lecturer(){
    }

    public static List<university> choice3 (Connection conn, List<university> listOfuni ){
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_lecturers;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
                        Lecturer a = new Lecturer();
                        a.setStaff_no(results.getInt(1));
                        a.setFname(results.getString(2));
                        a.setLname(results.getString(3));
                        a.setOffice(results.getString(4));
                        listOfuni.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }

    public static List<university> choice4(Connection conn, List<university> listOfuni){
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_teach;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
                        Lecturer a = new Lecturer();
                        a.setStaff_no(results.getInt(3));
                        a.setDept(results.getString(1));
                        a.setNumber(results.getString(2));
                        listOfuni.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }

    public static void main(String[] args) {
        Lecturer a = new Lecturer(1234,"asd", "54654");
        System.out.println( a.abstractinfor());
    }
}
