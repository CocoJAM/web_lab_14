package ictgradschool.web.lab14.ex3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by ljam763 on 4/05/2017.
 */
public class Student extends university{
    public Student(int id, String fname, String lname, String country, int mentor) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.country = country;
        this.mentor = mentor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getMentor() {
        return mentor;
    }

    public void setMentor(int mentor) {
        this.mentor = mentor;
    }

    private int id;
    private String fname;
    private String lname;
    private String country;
    private int mentor;

    @Override
    public String getbasicinfo() {
        return ("Student Id: " + id + ", Name: "+ fname+ " "+lname + ", nationality: " + country + ", mentor number: "+mentor);
    }

    @Override
    public String abstractinfor() {
        return ("Student Id: " + id + ", Name: "+ fname+ " "+lname );
    }

    public Student(){}
    public static List<university> choice5(Connection conn, List<university> listOfuni){
        try {
            try (PreparedStatement stat = conn.prepareStatement(
                    "SELECT * FROM unidb_students;"
            )) {
                try (ResultSet results = stat.executeQuery()) {
                    while (results.next()) {
                        Student c = new Student();
                        c.setId(results.getInt(1));
                        c.setFname(results.getString(2));
                        c.setLname(results.getString(3));
                        c.setCountry(results.getString(4));
                        c.setMentor(results.getInt(5));
                        listOfuni.add(c);
                    }

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfuni;
    }
}
