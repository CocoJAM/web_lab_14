package ictgradschool.web.lab14;

import java.util.Properties;

/**
 * Allows the program to get properties such as username & password from a centralized location.
 */
public class Config {

    public static final String DB_NAME = "your_db_name";

    private static Properties properties;

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties(System.getProperties());
            properties.setProperty("user", "ljam763");
            properties.setProperty("password", "b501347b");
            properties.setProperty("useSSL", "true");
        }
        return properties;
    }

}
