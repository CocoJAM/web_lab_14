package ictgradschool.web.lab14.examples.example4_parameterized_updates;

import ictgradschool.web.lab14.Config;

import java.sql.*;

public class Example04 {
    public static void main(String[] args) {

        // Load JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection("jdbc:mysql://mysql1.sporadic.co.nz:3306/" + Config.DB_NAME, Config.getProperties())) {
            System.out.println("Connection successful");

            // Insert a row. Note that this is just and example and we should ALWAYS use parameterized queries & updates (see other examples)!
            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO unidb_courses (dept, num, descrip, coord_no, rep_id) VALUES (?, ?, ?, ?, ?);")) {

                stmt.setString(1, "test");
                stmt.setInt(2, 123);
                stmt.setString(3, "A description");
                stmt.setInt(4, 666);
                stmt.setInt(5, 1713);

                int rowsAffected = stmt.executeUpdate();
                System.out.println(rowsAffected + " rows Inserted successfully");
            }

            // Delete a row. Note that this is just and example and we should ALWAYS use parameterized queries & updates (see other examples)!
            try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM unidb_courses WHERE dept = ? AND num = ?;")) {

                stmt.setString(1, "test");
                stmt.setInt(2, 123);

                int rowsAffected = stmt.executeUpdate();
                System.out.println(rowsAffected + " rows Deleted successfully");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
